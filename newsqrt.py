""" Module for computing sqrt with Newton's method """
def sqrt2(a,x0=1,debug=False):
    """Function for comupting the sqrt"""
    assert type(a) is int or type(a) is float, "error, input"
    assert a>=0, "error, input must be non-negative"
    x0 = 1
    tol = 1e-12
    maxit = 100000
    for i in range(maxit):
        x = x0/2 + a/(2*x0)
        delta_x = abs (x-x0)
        if debug:
            print("after iteration %d, x = %18.16f, dx = %18.16f" %(i+1,x,delta_x))

        if delta_x<tol:
            if debug:
                print("Converged")
            break
        x0 = x
    return x
